import { $ } from '@core/dom'

export class Excel {
  constructor(selector, options) {
    this.$root = $(selector)
    this.components = options.components || []
  }

  getRoot() {
    const $root = $.create('div', 'excel')

    this.components = this.components.map(Component => {
      const $node = $.create('div', Component.className)
      const component = new Component($node)
      $node.html(component.toHTML())
      $root.append($node)
      return component
    })

    return $root
  }

  render() {
    this.$root.append(this.getRoot())
    this.components.forEach(c => c.init())
  }
}
