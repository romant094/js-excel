import { $ } from '@core/dom'

export const resizeHandler = (event, $root) => {
  if (event.target.dataset.resize) {
    const $resizer = $(event.target)
    const $parent = $resizer.closest('[data-type="resizable"]')
    const { right, width, height, bottom } = $parent.getCoordinates()
    const { col } = $parent.data
    const tableItemType = $resizer.data.resize
    const dimProp = tableItemType === 'col'
      ? { height: '100vh' }
      : { width: '100vw' }
    const dimPropKey = Object.keys(dimProp)[0]
    let updatedDimValue
    $resizer.css({ opacity: 1, ...dimProp })

    document.onmousemove = e => {
      if (tableItemType === 'col') {
        const delta = e.pageX - right
        updatedDimValue = width + delta
        $resizer.css({ right: `${-delta}px` })
      } else {
        const delta = e.pageY - bottom
        updatedDimValue = height + delta
        $resizer.css({ bottom: `${-delta}px` })
      }
    }

    document.onmouseup = () => {
      document.onmousemove = null
      document.onmouseup = null
      $resizer.css({ opacity: 0 })
      if (tableItemType === 'col') {
        $root
          .findAll(`[data-col="${col}"]`)
          .forEach(el => {
            el.style.width = `${updatedDimValue}px`
          })
        $resizer.css({ right: 0 })
      } else {
        $parent.css({ height: `${updatedDimValue}px` })
        $resizer.css({ bottom: 0 })
      }
      $resizer.css({[dimPropKey]: '100%'})
    }
  }
}
