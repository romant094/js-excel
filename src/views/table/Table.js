import { shouldResize } from '@/views/table/table.functions'
import { resizeHandler } from '@/views/table/table.resize'
import { createTable } from '@/views/table/table.template'
import { ExcelComponent } from '@core/ExcelComponent'

export class Table extends ExcelComponent {
  static className = 'excel__table'

  constructor($root) {
    super($root, {
      listeners: ['mousedown'],
    });
  }

  toHTML() {
    return createTable()
  }

  onMousedown(event) {
    if (shouldResize(event)) {
      resizeHandler(event, this.$root)
    }
  }
}
