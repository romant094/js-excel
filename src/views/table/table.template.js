const CODES = {
  A: 65,
  Z: 90,
}

const createCell = (content = '', index) => {
  const type = content ? 'head' : 'basic'
  const cell = document.createElement('div')
  const classes = ['cell']
  if (type === 'head') {
    classes.push('cell--head')
    cell.dataset.type = 'resizable'
  } else {
    cell.contentEditable = 'true'
  }
  classes.forEach(clz => cell.classList.add(clz))
  cell.textContent = content
  if (type === 'head') {
    cell.insertAdjacentHTML(
      'beforeend',
      '<div class="cell__resize" data-resize="col"></div>'
    )
  }
  cell.dataset.col = index
  return cell.outerHTML
}

const createRow = (content, index) => {
  return `
    <div class='row' data-type="resizable">
      <div class='row__info'>
        ${index ? index : ''}
        ${index > 0 ? '<div class="row__resize" data-resize="row"></div>' : ''}
      </div>
      <div class='row__data'>${content}</div>
    </div>
  `
}

const generateRows = (
  rowsCount = 15,
  colsCount = 20,
  rows = [],
  index = 0
) => {
  if (rowsCount > 0) {
    const cols = generateCols(colsCount)
    return generateRows(
      rowsCount - 1,
      colsCount,
      [...rows, createRow(cols, index + 1)],
      index + 1
    )
  }
  return rows
}

const toChar = index => String.fromCharCode(CODES.A + index)

const generateCols = (colsCount, empty = true) => {
  return new Array(colsCount)
    .fill('')
    .map((item, index) => empty ? item : toChar(index))
    .map(createCell)
    .join('')
}

export const createTable = (
  rowsCount = 15,
) => {
  const colsCount = CODES.Z - CODES.A + 1
  const firstRowCols = generateCols(colsCount, false)
  const rows = generateRows(rowsCount, colsCount, [createRow(firstRowCols)])
  return rows.join('')
}
