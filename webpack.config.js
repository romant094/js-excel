const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const { NODE_ENV, PORT } = process.env
const isDev = NODE_ENV === 'development'
const isProd = !isDev

const resolvePath = url => path.resolve(__dirname, url)

const filename = fn => {
  if (isDev) {
    return fn
  }
  const [name, ext] = fn.split('.')
  return [name, '[hash]', ext].join('.')
}

const jsLoader = () => {
  const loaders = [
    {
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env'],
        plugins: ['@babel/plugin-proposal-class-properties'],
      },
    },
  ]

  if (isDev) {
    loaders.push('eslint-loader')
  }

  return loaders
}

module.exports = {
  context: resolvePath('src'),
  mode: 'development',
  entry: ['@babel/polyfill', './index.js'],
  output: {
    filename: filename('bundle.js'),
    path: resolvePath('build'),
  },
  resolve: {
    extensions: ['.js'],
    alias: {
      '@': resolvePath('src'),
      '@core': resolvePath('src/core'),
    },
  },
  devtool: isDev ? 'source-map' : false,
  devServer: {
    port: PORT,
    hot: isDev,
    liveReload: isDev,
    open: isDev,
    clientLogLevel: 'error',
  },
  target: 'web',
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: 'index.html',
      inject: 'body',
      minify: isProd,
    }),
    new CopyPlugin({
      patterns: [
        {
          from: resolvePath('src/assets/favicon.ico'),
          to: resolvePath('build'),
        },
      ],
    }),
    new MiniCssExtractPlugin({
      filename: filename('styles.css'),
    }),
  ],
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules)/,
        use: jsLoader(),
      },
    ],
  },
}
